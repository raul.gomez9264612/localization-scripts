import re


def process_schema(file_path):

    raw_text = ""
    with open(file_path, "r", encoding="utf-8") as f:
        raw_text = f.read()

    # Extract the slug
    pattern_slug = re.compile(r"SURVEY_SLUGS\.[A-Z_]+")
    slug = pattern_slug.search(raw_text).group(0)

    # Define the patterns
    pattern_title = re.compile(r"title:\s*'([^']+)'")
    pattern_all_labels = re.compile(r"id:\s*'([^']+)',\s*label:\s*('([^']+)')", re.DOTALL)
    answers_block_pattern = re.compile(
        r"\{\s*id:\s*'([^']*)'(?:[^{}]|{[^{}]*})*?answers:\s*(\[[^\]]*\])", re.DOTALL
    )

    # Define replacement functions
    def replace_title(match):
        return f"title: `surveyTitles:${{{slug}}}`"

    def replace_question_label(match):
        id_value = match.group(1) 
        return (
            "id: '"
            + id_value
            + f"',\n          label: `${{{slug}}}:questions.{id_value}.label`"
        )

    # Function to replace labels within answers blocks differently
    def replace_answers_labels(match): 
        question_id = match.group(1)
        pattern_label_nested = r"id:\s*'([^']+)',\s*label:\s*(`([^']+)`)"
        pattern_answers_section = r"answers:\s*(\[[^\]]*\])"

        def replace_answers_section(match):
            return re.sub(pattern_label_nested, replace_label_answer, match.group(0))

        def replace_label_answer(match):
            answer_id = match.group(1)
            return (
                "id: '"
                + answer_id
                + f"',\n          label: `${{{slug}}}:questions.{question_id}.answers.{answer_id}`"
            )
        
        return re.sub(pattern_answers_section, replace_answers_section, match.group(0))

    # Replace the title
    temp_1 = re.sub(pattern_title, replace_title, raw_text)
    # Replace all labels, independently of whether question or answer
    temp_2 = re.sub(pattern_all_labels, replace_question_label, temp_1)
    # Isolate answers, and replace their labels again
    temp_3 = re.sub(answers_block_pattern, replace_answers_labels, temp_2)

    return temp_3
