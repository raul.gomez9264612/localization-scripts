# Localization scripts

To get started localizing at the speed of light, one needs to do first the following

## Prerequirements 

1. Install the dependencies from requirements.txt
2. Go to https://platform.openai.com/ and create a openAI API key. This is needed for the automated translations to English
3. Add your key to the .env file
4. Drag the surveys you want to translate to the /input folder

## Executing the script
As simple as it gets

`python main.py`

## Caveats
- Please make sure that the survey has a defined slug in the format SURVEY_SLUGS.*, otherwise the regex won't catch it
- Note that the scripts uses a dump with the hydrated survey objects from the browser (surveys.csv) and one of the questions that appear in constants.js (constants.json) to prevent repeating over and over again the same translations. Therefore, you can expect that the script won't work for newly added surveys
- Note that this script saves hours of manual work, but it still is expected that a human integrates and debugs the resulting files