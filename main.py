import pandas as pd
import os
import json
import re
from regex_logic import process_schema
from openai import OpenAI
from tenacity import retry, stop_after_attempt
from io import TextIOWrapper
from dotenv import load_dotenv

load_dotenv()

client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY"))

root_folder = "./data"
cts_file = "constants.json"
surveys_file = "surveys.csv"
surveys_folder = "./data/surveys_copy/"
new_folder = "./data/surveys6/"
title_pattern = re.compile(r"title:\s*'([^']+)'")
child_pattern = re.compile(r'slug:.*_CHILD,')

# These are the questions of constants.js. We want to prevent processing them repeatedly
cts = None
with open(os.path.join(root_folder, cts_file), "r") as f:
    cts = json.load(f)
ct_questions = list(cts.keys())

# Builds a dataframe from a csv file with two columns: 'object' and 'slug'.
# Object is the serialized JSON object..
surveys = pd.read_csv(os.path.join(root_folder, surveys_file))
surveys['object'] = surveys['object'].apply(json.loads)


def find_questions(obj):
    """
    Generator that yields the 'questions' key in a nested dictionary or list.

    Args:
        obj (dict or list): The object to search in.

    Yields:
        Any: The values associated with the 'questions' key.

    """
    if isinstance(obj, dict):  # If the object is a dictionary
        for key, value in obj.items():
            if key == "questions":  # Check if the key is 'question'
                yield value
            else:
                yield from find_questions(value)  # Recursively search in value
    elif isinstance(obj, list):  # If the object is a list
        for item in obj:
            yield from find_questions(item)  # Recursively search in each item


def extract_localization_strings(survey):
    """
    Extract questions and aliases from a survey object and convert them into a cleaned dictionary format.
    Note that questions in constants.js are filtered out, 
    as these are part of the constants localizaiton namespace, not the specific survey.

    Args:
        survey (dict): The survey object containing questions and aliases.

    Returns:
        dict: A dictionary with cleaned questions and aliases.
    """
    survey = survey["object"]

    questions = []
    for question in find_questions(survey):
        questions.extend(question)

    questions_filtered = list(filter(lambda x: x['id'] not in ct_questions, questions))

    localization_data = {}
    for question in questions_filtered:
        question_data = {}
        if "infoMessage" in question:
            question_data["infoMessage"] = question["infoMessage"]
        if "tooltip" in question:
            question_data["tooltip"] = question["tooltip"]
        if "answers" in question:
            answers = {answer["id"]: answer["label"] for answer in question["answers"]}
            question_data["answers"] = answers

        localization_data[question['id']] = {
            "label": question['label'],
            **question_data
        }

    output_data = {
        "questions": localization_data
    }

    if 'alias' in survey:
        output_data["alias"] = survey['alias']

    print(f"Extracted questions and aliases from survey {survey['slug']} 🚀")
    print("Put them in the right JSON format")

    return output_data


def get_questions_and_aliases(survey):
    """
    Extracts questions and aliases from a survey.

    Args:
        survey (dict): The survey object containing questions and aliases.

    Returns:
        dict: A dictionary containing the extracted questions and aliases (if available).
    """
    questions = []
    for question in find_questions(survey):
        questions.extend(question)
    questions_filtered = list(
        filter(lambda x: x['id'] not in ct_questions, questions))
    out_dict = {"questions": questions_filtered}
    if 'alias' in survey:
        out_dict["alias"] = survey['alias']
    print(f"Extracted questions and aliases from survey {survey['slug']} 🚀")
    return out_dict


def extract_localization_data_from_survey(survey):
    """
    Extract questions and aliases from a survey object and convert them into a cleaned dictionary format.

    Args:
        survey (dict): The survey object containing questions and aliases.

    Returns:
        dict: A dictionary with cleaned questions and aliases.
    """
    questions = {}

    for question in survey["questions"]:
        question_data = {}

        if "infoMessage" in question:
            question_data["infoMessage"] = question["infoMessage"]

        if "tooltip" in question:
            question_data["tooltip"] = question["tooltip"]

        if "answers" in question:
            answers = {answer["id"]: answer["label"]
                       for answer in question["answers"]}
            question_data["answers"] = answers

        questions[question['id']] = {
            "label": question['label'], **question_data}

    localization_data = {
        "questions": questions
    }

    if "alias" in survey:
        localization_data["alias"] = survey["alias"]

    print("Put them in the right JSON format")
    return localization_data


def translate_dict_to_english(dict_in, openai_client, model="gpt-3.5-turbo"):
    """
    Translates the values of a JSON object to English while leaving the keys intact.
    Also translates the strings in arrays. Returns the translated object as raw text.

    Args:
        dict_in (dict): The input JSON object to be translated.
        openai_client: The OpenAI client used for translation.
        model (str, optional): The model to use for translation. Defaults to "gpt-3.5-turbo".

    Returns:
        str: The translated JSON object as raw text.
    """
    dict_to_translate = json.dumps(dict_in)
    print("Machines translating it for you. Beep! 🤖")
    completion = openai_client.chat.completions.create(
        model=model,
        messages=[
            {"role": "system", "content": "You are a helpful assistant. You help transform JSON files. You never provide additional context, only code. You don't give markdown specification of the code, or any way to specify code blocks. Just the code as raw text."},
            {"role": "user", "content": f"Please translate the values of my JSON object to english. Please leave the keys intact. Also translate the strings in arrays. Give me only the object as raw text. No more words. \n ---- {dict_to_translate}"}
        ]
    )
    print("Translated to English! ✅")
    print(completion.choices[0].message.content)
    return completion.choices[0].message.content


@retry(stop=stop_after_attempt(5))
def translate_with_retry(dict_in, openai_client, model="gpt-3.5-turbo"):
    """
    Translates a dictionary to English using the OpenAI GPT-3.5 Turbo model with retry mechanism.

    Args:
        dict_in (dict): The input dictionary to be translated.
        openai_client: The OpenAI client object used for translation.
        model (str, optional): The name of the OpenAI model to use for translation. Defaults to "gpt-3.5-turbo".

    Returns:
        dict: The translated dictionary in JSON format.
    """
    json_translation = json.loads(
        translate_dict_to_english(dict_in, openai_client, model))
    return json_translation


def process_survey(survey, source_path, source_filename):
    """
    Create a survey folder, process the schema file to insert localization keys, 
    and create files for localization strings in English and German.

    Args:
        survey (dict): The survey object containing survey details.
        source_path (str): The path to the directory containing the survey
        file.
        source_filename (str): The name of the survey file.

    Returns:
        None
    """
    survey_slug = survey['slug']
    output_path = os.path.join(new_folder, survey_slug)
    source_file_path = os.path.join(source_path, source_filename)

    if not os.path.exists(output_path):
        os.makedirs(output_path)

        with open(os.path.join(output_path, f"{survey_slug}.schema.js"), "w+") as schema_file:
            schema_content = process_schema(source_file_path)
            schema_file.write(schema_content)

        german_translations = extract_localization_strings(survey)
        with open(os.path.join(output_path, "de.json"), "w") as german_file:
            json.dump(german_translations, german_file,
                      indent=4, ensure_ascii=False)

        english_translations = translate_with_retry(
            german_translations, client)
        with open(os.path.join(output_path, "en.json"), "w") as english_file:
            json.dump(english_translations, english_file, indent=4)

        print(f"Processed and created folders for {survey_slug}")
        
    else:
        print(f"Folder already exists for {survey_slug}")


def extract_title_and_child_flag(file: TextIOWrapper):
    """
    Extracts the title and child flag from a schema file.
    """
    title = None
    is_child_survey = False

    previous_line = None
    for row in file:
        if previous_line is not None:
            is_child_survey = bool(
                child_pattern.search(previous_line))
        match = title_pattern.search(row)
        previous_line = row
        if match:
            title = match.group(1)
            break  # Stop reading further once the title is found

    return title, is_child_survey


def main():
    """
    Traverses the surveys folder and processes the schema files.
    """
    count_files = 0
    processed_surveys = set()

    for path, dir, file in os.walk(surveys_folder):
        for f in file:
            count_files += 1
            if f.endswith(".js"):
                with open(os.path.join(path, f), "r") as f_deep:
                    title, is_child_survey = extract_title_and_child_flag(
                        f_deep)
                    # Match the files in the folder with the surveys in the CSV
                    # As we match by title, the child surveys may also be added
                    matching_surveys = surveys[surveys['object'].apply(
                        lambda x: x['title'] == title)]

                    if len(matching_surveys) > 0:
                        if is_child_survey:
                            # Looking specifically for a child survey
                            child_surveys = matching_surveys[matching_surveys['object'].apply(
                                lambda x: "child" in x['slug'])]
                            if not child_surveys.empty:
                                survey = child_surveys.iloc[0]
                            else:
                                print("No child survey found.")
                                continue
                        else:
                            survey = matching_surveys[matching_surveys['object'].apply(
                                lambda x: "child" not in x['slug'])].iloc[0]

                        if survey['slug'] not in processed_surveys:
                            process_survey(survey, path, f)
                            processed_surveys.add(survey['slug'])
                    else:
                        print(f"Could not find matching survey for {f}")


if __name__ == "__main__":
    created_folders = 0
    main()
    print(f"Created {created_folders} folders 🎉")
